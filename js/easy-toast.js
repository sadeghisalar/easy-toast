/* d8 a8 d8 b3 d9 85 20 d8 a7 d9 84 d8 b1 d8 a8 20 d8 a7 d9 84 d8 ad d8 b3 db 8c d9 86 */
// EasyToast
// version 1.0.0
// ©SalarSadeghi, 2020
// https://gitlab.com/sadeghisalar
// GitLab Page:https://gitlab.com/sadeghisalar/easy-toast
// Released under GNU licence
// =========================================================
var EasyToast = /** @class */ (function () {
    function EasyToast(theme) {
        this.theme = theme;
        this.theme = theme;
        this.createContainer();
        this._buttons = [];
        return this;
    }
    Object.defineProperty(EasyToast.prototype, "container", {
        get: function () {
            return this._container;
        },
        set: function (value) {
            this._container = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(EasyToast.prototype, "buttons", {
        get: function () {
            return this._buttons;
        },
        set: function (value) {
            this._buttons.push(value);
        },
        enumerable: true,
        configurable: true
    });
    EasyToast.prototype.getBody = function () {
        return this.body;
    };
    /**
     * Set EasyToast Body Text
     * @param {(string)} value Body text/Html,
     * @return {this} EasyToast.
     */
    EasyToast.prototype.setBody = function (value) {
        this.body = value;
        return this;
    };
    EasyToast.prototype.getAlign = function () {
        var _a;
        return (_a = this.align) !== null && _a !== void 0 ? _a : 'center-bottom';
    };
    /**
     * Set EasyToast Alignment To Container, default: center-bottom
     * @param {(string)} align center-top, center-bottom,
     * left-top, left-right,
     * right-top, right-bottom.
     * @return {this} String EasyToast Alignment.
     */
    EasyToast.prototype.setAlign = function (align) {
        this.align = align;
        return this;
    };
    /**
     * Set EasyToast Close Button
     * @param {(string|boolean)} closeButton Button Text | Disable/Enable.
     * @return {this} EasyToast.
     */
    EasyToast.prototype.setCloseButton = function (closeButton) {
        this.closeButton = closeButton;
        return this;
    };
    EasyToast.prototype.getCloseButton = function () {
        var _a;
        if (typeof this.closeButton !== 'boolean')
            return (_a = this.closeButton) !== null && _a !== void 0 ? _a : 'X';
        return this.closeButton;
    };
    /**
     * Set EasyToast Auto Close
     * @param {(boolean)} autoClose Disable/Enable.
     * @param {(boolean)} autoCloseTime Close After N milliseconds, Default : 1500.
     * @return {this} EasyToast.
     */
    EasyToast.prototype.setAutoClose = function (autoClose, autoCloseTime) {
        this.autoClose = autoClose;
        this.autoCloseTime = autoCloseTime;
        return this;
    };
    EasyToast.prototype.getAutoClose = function () {
        var _a;
        return (_a = this.autoClose) !== null && _a !== void 0 ? _a : true;
    };
    EasyToast.prototype.getAutoCloseTime = function () {
        var _a;
        return (_a = this.autoCloseTime) !== null && _a !== void 0 ? _a : 1500;
    };
    /**
     * EasyToast Close Callback
     * @param {(function)} callback Fire When EasyToast Closed.
     * @return {this} EasyToast.
     */
    EasyToast.prototype.setAfterClose = function (callback) {
        this._afterClose = callback;
        return this;
    };
    EasyToast.prototype.getAfterClose = function () {
        // @ts-ignore
        return this._afterClose === undefined ? function () { } : this._afterClose;
    };
    /**
     * EasyToast Auto Close Callback
     * @param {(function)} callback Fire When EasyToast Close Automatically.
     * @return {this} EasyToast.
     */
    EasyToast.prototype.setAfterAutoClose = function (callback) {
        this._afterAutoClose = callback;
        return this;
    };
    EasyToast.prototype.getAfterAutoClose = function () {
        // @ts-ignore
        return this._afterAutoClose === undefined ? function () { } : this._afterAutoClose;
    };
    /**
     * EasyToast Custom Button
     * @param {(string)} body Button Text.
     * @param {(function(context))} onclick Event Listener.
     * @param {([key: string]: string)} attribute set attribute to button.
     * @return {this} EasyToast.
     */
    EasyToast.prototype.createButton = function (body, onclick, attribute) {
        var self = this;
        var element = document.createElement('button');
        element.setAttribute('type', 'button');
        element.innerHTML = body !== null && body !== void 0 ? body : '!';
        for (var i = 0; i < Object.keys(attribute).length; i++) {
            var key = Object.keys(attribute)[i];
            element.setAttribute(key, attribute[key]);
        }
        element.addEventListener('click', function (e) {
            onclick(self);
        });
        this.buttons = element;
        return this;
    };
    EasyToast.prototype.createContainer = function () {
        var element = document.createElement('div');
        element.setAttribute('class', 'easy-toast-container easy-toast-' + this.theme);
        this.container = element;
        this.container.animationIn = function (self) {
            self.container.classList.add('slide-in-' + self.getAlign());
        };
        this.container.animationOut = function (self) {
            self.container.classList.remove('slide-in-' + self.getAlign());
            self.container.classList.add('slide-out-' + self.getAlign());
        };
        return element;
    };
    EasyToast.prototype.createCloseButton = function () {
        if ((this.getCloseButton()) == false)
            return document.createElement('b');
        var self = this;
        var element = document.createElement('span');
        element.setAttribute('class', 'easy-toast-close');
        element.innerHTML = (this.getCloseButton());
        element.addEventListener('click', function (e) {
            self.container.animationOut(self);
            setTimeout(function () {
                self.container.remove();
                self.getAfterClose()();
            }, 300);
        });
        return element;
    };
    EasyToast.prototype.createBody = function () {
        var element = document.createElement('p');
        element.setAttribute('class', 'easy-toast-body');
        element.innerHTML = this.getBody();
        return element;
    };
    EasyToast.prototype.createButtonsContainer = function () {
        var element = document.createElement('div');
        element.setAttribute('class', 'easy-toast-buttons-container');
        return element;
    };
    EasyToast.prototype.closeAutomatically = function () {
        var self = this;
        if (!this.getAutoClose())
            return;
        var ll = document.querySelectorAll('.easy-toast-container').length;
        setTimeout(function () {
            self.container.animationOut(self);
            setTimeout(function () {
                self.container.remove();
                self.getAfterAutoClose()();
            }, 300);
        }, this.getAutoCloseTime());
    };
    EasyToast.prototype.closeAll = function () {
        document.querySelectorAll('.easy-toast-container').forEach(function (toast) {
            toast.remove();
        });
    };
    EasyToast.prototype.build = function () {
        this.container.classList.add('easy-toast-align-' + this.getAlign());
        this.container.animationIn(this);
        this.container.append(this.createCloseButton());
        var buttonsContainer = this.createButtonsContainer();
        this.container.append(this.createBody());
        this.container.append(buttonsContainer);
        this.buttons.forEach(function (item) {
            buttonsContainer.append(item);
        });
        document.body.append(this.container);
        //closeAutomatically
        this.closeAutomatically();
    };
    /**
     * Success Theme
     * @return {this} EasyToast.
     */
    EasyToast.getSuccess = function () {
        var self = new EasyToast('success');
        self.closeAll();
        return self;
    };
    /**
     * Danger Theme
     * @return {this} EasyToast.
     */
    EasyToast.getDanger = function () {
        var self = new EasyToast('danger');
        self.closeAll();
        return self;
    };
    /**
     * Info Theme
     * @return {this} EasyToast.
     */
    EasyToast.getInfo = function () {
        var self = new EasyToast('info');
        self.closeAll();
        return self;
    };
    /**
     * Warning Theme
     * @return {this} EasyToast.
     */
    EasyToast.getWarning = function () {
        var self = new EasyToast('warning');
        self.closeAll();
        return self;
    };
    /**
     * Light Theme
     * @return {this} EasyToast.
     */
    EasyToast.getLight = function () {
        var self = new EasyToast('light');
        self.closeAll();
        return self;
    };
    /**
     * Dark Theme
     * @return {this} EasyToast.
     */
    EasyToast.getDark = function () {
        var self = new EasyToast('dark');
        self.closeAll();
        return self;
    };
    /**
     * set Custom Theme
     * @param {{string}} theme set theme name.
     * @return {this} EasyToast.
     */
    EasyToast.getCustomTheme = function (theme) {
        var self = new EasyToast(theme);
        self.closeAll();
        return self;
    };
    /**
     * Show EasyToast
     */
    EasyToast.prototype.show = function () {
        this.build();
    };
    return EasyToast;
}());
