# EasyToast
Create Easy (And Advanced) Vanilla Js Toast

# Demo

[Check Out Demo](https://sadeghisalar.gitlab.io/easy-toast-page)

# Get Started

### 1
```php
 git clone https://gitlab.com/sadeghisalar/easy-toast.git
```
### 2
```html
<link rel="stylesheet" href="/path/to/easy-toast.css">
<script src="/path/to/easy-toast.js"></script>
```

### 3
```javascript
EasyToast.getDanger().setBody('Hello World').show()
```
# Documentation

## Themes
```javascript
/**
* Success Theme
* return EasyToast
*/
getSuccess()

//Example:
EasyToast.getSuccess().setBody('Hello Info').show()
```

```javascript
/**
* Danger Theme
* return EasyToast
*/
getDanger()

//Example:
EasyToast.getDanger().setBody('Hello Danger').show()
```

```javascript
/**
* Info Theme
* return EasyToast
*/
getInfo()

//Example:
EasyToast.getInfo().setBody('Hello Info').show()
```

```javascript
/**
* Warning Theme
* return EasyToast
*/
getWarning()

//Example:
EasyToast.getWarning().setBody('Hello Warning').show()
```

```javascript
/**
* Light Theme
* return EasyToast
*/
getLight()

//Example:
EasyToast.getLight().setBody('Hello Light').show()
```

```javascript
/**
* Dark Theme
* return EasyToast
*/
getDark()

//Example:
EasyToast.getDark().setBody('Hello Dark').show()
```

```javascript
/**
* Custom Theme
* return EasyToast
*/
getCustomTheme(theme:string)

//Example:
EasyToast.getCustomTheme('purple').setBody('Hello purple').show()
```

## Alignment : Optional
```javascript
/**
* Set EasyToast Alignment
* param string  align center-top, center-bottom,
* left-top, left-right,
* right-top, right-bottom.
* default center-bottom
* return EasyToast
*/
setAlign(align: string)

//Example:
EasyToast.getLight().setAlign('left-top').setBody('Hello World').show()
```
## Body
```javascript
/**
* Set EasyToast Body Content
* param string value Body,
* return EasyToast.
*/
setBody(value: string)

//Example:
EasyToast.getLight().setBody('My Body Test'+Date.now()).show()
```

## CloseButton : Optional
```javascript
/**
* Set EasyToast Close Button
* param string|boolean: Button Text | true,false.
* return EasyToast
* default true
* return EasyToast
*/
setCloseButton(closeButton: string|boolean)

//Example 1:
EasyToast.getLight().setCloseButton('X').setBody('Hello World').show()

//Example 2:
EasyToast.getLight().setCloseButton(false).setBody('Hello World').show()
```

## AutoClose : Optional

```javascript
/**
* Set EasyToast Auto Close
* param boolean autoClose true/false. Optional , Default true
* param boolean autoCloseTime Close After N milliseconds, Default : 1500. Optional
* return EasyToast
*/
setAutoClose(autoClose:boolean,autoCloseTime:number)

//Example1:
EasyToast.getLight()
	.setAutoClose(false).setBody('Hello World').show()

//Example2:
EasyToast.getLight()
    .setAutoClose(true,2000).setBody('Hello World').show()
```
## Close CallBack : Optional
```javascript
/**
* Set EasyToast Close CallBack
* param function Fire When EasyToast get Removed.
* return EasyToast
*/
setAfterClose(callback: () => {})

//Example:
EasyToast.getLight()
	.setAfterClose(() => {alert('hi')}).setBody('Hello World').show()
```

## Auto Close CallBack : Optional
```javascript
/**
* Set EasyToast Close CallBack
* param function Fire When EasyToast get Removed Automatically.
* return EasyToast
*/
setAfterAutoClose(callback: () => {})

//Example:
EasyToast.getLight()
	.setAfterAutoClose(() => {alert('hi')}).setBody('Hello World').show()
```
## Create Custom Button(s) : Optional
```javascript
/**
* Set EasyToast Custom Button(s)
* param string Button Text.
* param function(context : Optional) onclick Event Listener.
* param [key: string]: string set attributes to button. Optional
* return EasyToast
*/
createButton(body:string,onclick:(context)=>{},attribute:{[key: string]: string} )

//Example:
EasyToast.getLight().setBody('Hello World')
    .createButton('Kick Me',
	() => { EasyToast.getDark().setBody('OK 🎃').show() },
	{'title' : 'Some Title', 'class' : 'some-class'})
	.createButton('Kiss Me',
	() => { EasyToast.getDark().setBody('No 🎆').show() },
	{'title' : 'Some Title', 'class' : 'some-class'})
	.show()
```